# lib32

鼠键事件

窗体启动

A星寻路

主循环

# lib5

游戏需要用到的资源

# libasm
生成obj（libwindsoul需要）

# libass

bass音频库 (http://www.un4seen.com/)

# libfolder

检测模块（from 浪西游资料片）

# libjpeg

jpeg图像解码库(by云风 Cloud Wu)


# libm5
梦幻群侠传5（from 子犯）

梦幻群侠传5.日期版


# library
静态/动态库

# libcc/libutil
一些组件/零件/基础方法/基础类

# libwindsoul

风魂引擎 (http://www.codingnow.com/windsoul/wpp16.cab)


# 示意图
![1](https://gitee.com/ulxy/libm5/raw/master/demo1.png)

![1](https://gitee.com/ulxy/libm5/raw/master/demo2.png)

# 贴吧

https://tieba.baidu.com/p/6179698081

# 开发环境

Visual Studio Community 2017

![安装器](https://gitee.com/ulxy/diagram_images/raw/master/installer.png)